#!/bin/bash -e

if [[ -z ${POM_G} ]]; then
    echo "Please export POM_G in your env"
    exit 1
fi

if [[ -z ${POM_A} ]]; then
    echo "Please export POM_A in your env"
    exit 1
fi

if [[ -z ${POM_V} ]]; then
    echo "Please export POM_V in your env"
    exit 1
fi
if [[ -z ${T_UUID} ]]; then
    echo "Please export T_UUID in your env"
    exit 1
fi
if [[ -z ${A_UUID} ]]; then
    echo "Please export A_UUID in your env"
    exit 1
fi
if [[ -z ${SERVICE_ID} ]]; then
    echo "Please export SERVICE_ID in your env"
    exit 1
fi
if [[ -z ${nexus_redirect} ]]; then
    echo "Please export nexus_redirect in your env"
    exit 1
fi

WORK_TIME=$(date +%Y-%m-%d_%H-%M-%S)
APISERVER_URL=${APISEVER_URL:-"http://10.1.2.183:8080"}
POM_R=${POM_R:-"snapshots"}

echo "download the war...."
rm -rf war &&mkdir -p war

mkdir -p dockerfile

WAR_NAME=${POM_A}-${POM_V}.war


wget "$nexus_redirect?r=${POM_R}&g=${POM_G}&a=${POM_A}&v=${POM_V}&e=war" -O war/$WAR_NAME

#wget "$nexus_redirect?r=$pom_r&g=$pom_g&a=$pom_a&v=$pom_v&e=war" -O $war

target_name=${POM_A}-${POM_V}-${WORK_TIME}
target_d=war/${target_name}
target_dir=`pwd`/$target_d
if [ ! -f "war/${WAR_NAME}" ]; then
	echo "war not exist: ${WAR_NAME}"
	exit 1
fi
 unzip -q  war/${WAR_NAME} -d war/$target_name   



echo "FROM   dordoka/tomcat
MAINTAINER zhengtao.wuzt <zhengtao.wuzt@ongo360.com>
RUN mkdir -p /opt/webull
ADD ./war/${target_name}  /opt/webull
ADD ./ROOT.xml /opt/tomcat/conf/Catalina/localhost/" > dockerfile/Dockerfile.${target_name}

docker build --no-cache -f   dockerfile/Dockerfile.${target_name} \
    -t registry.cn-hangzhou.aliyuncs.com/zanecloud/${POM_A}:${POM_V}-${WORK_TIME} .

 docker push registry.cn-hangzhou.aliyuncs.com/zanecloud/${POM_A}:${POM_V}-${WORK_TIME}

# ./autodeploy start   --apiserver_host=10.10.1.183    --template_uuid=$T_UUID  --application_uuid=$A_UUID \
#       --service_name=$SERVICE_ID --image_name=registry.cn-hangzhou.aliyuncs.com/zanecloud/${POM_A}   --image_tag=${POM_V}-${WORK_TIME} --user=root --pass=hell05a

